###
# Copyright (c) 2021, Georg Pfuetzenreuter
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import os
from supybot import utils, plugins, ircutils, callbacks, ircdb, ircmsgs
from supybot.commands import *
from supybot.ircmsgs import nick

try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('System')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    "_ = lambda x: x"

class System(callbacks.Plugin):
    """Internal Use"""
    threaded = True
    pass

    def system(self, irc, msg, args, option1, option2):
        """<option> <option>
        If you're supposed to use this you know how."""
    
        nick = msg.nick
        hostmask = irc.state.nickToHostmask(msg.nick)

        if 'get' in 'option1':
            if 'all' in 'option2':
                if hostmask in read or hostmask in write:
                    command = "/home/georg/botproc.pl"
                    command_output = os.popen(command)
                    for line in command_output:
                        irc.reply(line.rstrip())
                else:
                    irc.reply("Fuck off.")
                    print("Intrustion attempt: " + hostmask)
            else:
                irc.reply("Your arguments smell bad.")
        elif 'noget' in 'option1':
                if 'all' in 'option2':
                  if hostmask in read or hostmask in write:
                        irc.reply("Implement this.")
                  else:
                        irc.reply("Go home.")
                        print("Intrusion attempt: " + hostmask)
                else:
                    irc.reply("Ew, stinky arguments.")
        else:
            irc.reply("What.")
    
        system = wrap(system, ['anything', 'anything'])


Class = System


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
